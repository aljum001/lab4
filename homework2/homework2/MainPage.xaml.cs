﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Diagnostics;

namespace homework2
{
    public partial class MainPage : ContentPage
    {

        public MainPage()
        {
            InitializeComponent();
        }

        int current = 0;
        string mathOP = "";
        double first, second, num;
        string strNum = "";
        string strNum2 = "";
        
        void allClear(object sender, EventArgs e)
        {
            current = 0;
            first = second = num = 0;
            Result.Text = "0";
            strNum = "";
            strNum2 = "";
        }

        void eClear()
        {
            current = 0;
            first = second = num = 0;
            strNum = "";
            strNum2 = "";
        }

        void OPclick(object sender, EventArgs e)
        {
            if ((current == 0) || (current == 5))
            {
                Button btn = (Button)sender;
                mathOP = btn.Text;
                current=1;
            }
        }

        void numClick(object sender, EventArgs e)
        {

            Button btn2 = (Button)sender;
            if(current == 3)
            {
                eClear();
            }
            if((current == 0) && (strNum.Length < 9))
            {
                strNum += btn2.Text;
                num = Int32.Parse(strNum);
                first = num;

            }
            else if (current == 3)
            {
                eClear();
            }
            else if ((current >= 1) && (strNum2.Length < 9))
            {
                strNum2 += btn2.Text;
                num = Int32.Parse(strNum2);
                second = num;
                current=2;
            }
            Result.Text = num.ToString();
        }

        void equalClick(object sender, EventArgs e)
        {
            if(current == 2)
            {
                double cResult = Calculate.calculator(first, second, mathOP);
                if ((cResult.ToString().Length > 9) && (mathOP != "/"))
                {   //fix this area. Do 80/90 and see the error
                    Result.Text = "error";
                    current = 3;
                }
                else
                {
                    Result.Text = cResult.ToString();
                    first = cResult;
                    strNum2 = "";
                    current = 5;
                }
            }
        }

        
    }
}
