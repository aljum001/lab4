﻿using System;

namespace homework2
{
    public static class Calculate
	{
        public static double calculator(double num1, double num2, string op)
    {
        double result = 0;
        if (op == "+")
        {
            result = num1 + num2;
        }
        else if (op == "-")
        {
            result = num1 - num2;
        }
        else if (op == "*")
        {
            result = num1 * num2;
        }
        else if (op == "/")
        {
            result = num1 / num2;
        }
        return result;
    }
}
}